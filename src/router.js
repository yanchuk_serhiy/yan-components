import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/buttons',
      name: 'buttons',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "buttons" */ './views/Buttons-view.vue')
    },
    {
      path: '/alerts',
      name: 'alerts',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "buttons" */ './views/Alerts-view.vue')
    },
    {
      path: '/inputs',
      name: 'inputs',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "buttons" */ './views/Input-view.vue')
    },
    {
      path: '/checkbox',
      name: 'checkbox',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "buttons" */ './views/Checkbox-view.vue')
    },
    {
      path: '/carousel',
      name: 'carousel',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "buttons" */ './views/Carousel-view.vue')
    },
    {
      path: '/modals',
      name: 'modals',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "buttons" */ './views/Modals-view.vue')
    }
  ]
})
