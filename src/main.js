import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vue2TouchEvents from 'vue2-touch-events'
 
Vue.use(Vue2TouchEvents)

import ybtn from './components/buttons/Buttons.vue'
Vue.use(ybtn)
Vue.component('ybtn', ybtn)

import circleBtn from './components/buttons/CircleButtons.vue'
Vue.use(circleBtn)
Vue.component('circleBtn', circleBtn)

import yalert from './components/alerts/Alerts.vue'
Vue.use(yalert)
Vue.component('yalert', yalert)

import yinput from './components/inputs/Input.vue'
Vue.use(yinput)
Vue.component('yinput', yinput)

import cinput from './components/inputs/CustomInput.vue'
Vue.use(cinput)
Vue.component('cinput', cinput)

import ymodal from './components/modals/Modal.vue'
Vue.use(ymodal)
Vue.component('ymodal', ymodal)

import amodal from './components/modals/AlertModal.vue'
Vue.use(amodal)
Vue.component('amodal', amodal)

import ycarousel from './components/carousel/Carousel.vue'
Vue.use(ycarousel)
Vue.component('ycarousel', ycarousel)

import ycheckbox from './components/checkbox/Checkbox.vue'
Vue.use(ycheckbox)
Vue.component('ycheckbox', ycheckbox)

import yradio from './components/checkbox/Radio.vue'
Vue.use(yradio)
Vue.component('yradio', yradio)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
